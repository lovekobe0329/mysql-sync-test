package com.huang.test.listener;

import com.huang.test.service.BinlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author qmhuang
 * @date 2020/8/4
 * @since 1.0.0
 */
@Component
public class BinlogClientRunner implements CommandLineRunner {
    @Autowired
    BinlogService binlogService;


    @Override
    public void run(String... args) throws Exception {
        binlogService.startListener();
    }
}
