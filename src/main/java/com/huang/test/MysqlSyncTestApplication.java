package com.huang.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlSyncTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqlSyncTestApplication.class, args);
    }

}
