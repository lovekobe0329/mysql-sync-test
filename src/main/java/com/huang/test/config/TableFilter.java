package com.huang.test.config;

import lombok.Data;

@Data
public class TableFilter {
    private String includes;
    private String excludes;
}