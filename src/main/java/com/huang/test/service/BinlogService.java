package com.huang.test.service;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author qmhuang
 * @date 2020/8/4
 * @since 1.0.0
 */
public interface BinlogService {
    void startListener() throws IOException;
}
