package com.huang.test.service.impl;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.*;
import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer;
import com.huang.test.service.BinlogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author qmhuang
 * @date 2020/8/4
 * @since 1.0.0
 */
@Service
@Slf4j
public class BinlogServiceImpl implements BinlogService {
    @Value("${binlog.mysql.host}")
    String host;
    @Value("${binlog.mysql.port}")
    int port;
    @Value("${binlog.mysql.username}")
    String username;
    @Value("${binlog.mysql.password}")
    String password;
    @Value("${binlog.mysql.server-id}")
    int serverId;
    @Value("${binlog.mysql.database-tables}")
    String databaseTables;



    @Async
    @Override
    public void startListener() throws IOException {
        BinaryLogClient client = new BinaryLogClient(host, port, username, password);

        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setCompatibilityMode(
                EventDeserializer.CompatibilityMode.DATE_AND_TIME_AS_LONG,
                EventDeserializer.CompatibilityMode.CHAR_AND_BINARY_AS_BYTE_ARRAY
        );

        HashMap<Long, String> tableMap = new HashMap<>();
        client.setServerId(serverId);
        client.setEventDeserializer(eventDeserializer);
        client.registerEventListener(new BinaryLogClient.EventListener() {
            @Override
            public void onEvent(Event event) {
                // binlog事件
                EventData data = event.getData();
                if (data != null) {
                    if (data instanceof TableMapEventData) {
                        TableMapEventData tableMapEventData = (TableMapEventData) data;
                        tableMap.put(tableMapEventData.getTableId(), tableMapEventData.getDatabase() + "." + tableMapEventData.getTable());
                    }
                    // update数据
                    if (data instanceof UpdateRowsEventData) {
                        List<Long> tableKeys = new ArrayList<>();
                        UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) data;
                        String tableName = tableMap.get(updateRowsEventData.getTableId());
                        if (filterData(tableName)) {
                            String eventKey = tableName + ".update";
                            for (Map.Entry<Serializable[], Serializable[]> row : updateRowsEventData.getRows()) {
                                Long key = Long.valueOf(row.getKey()[0]+"");
                                tableKeys.add(key);
                            }
                        }

                        log.info("update data : {}", tableKeys);
                    }
                    // insert数据
                    else if (data instanceof WriteRowsEventData) {
                        WriteRowsEventData writeRowsEventData = (WriteRowsEventData) data;
                        String tableName = tableMap.get(writeRowsEventData.getTableId());
                        if (filterData(tableName)) {
                            String eventKey = tableName + ".insert";

                        }
                    } // delete数据
                    else if (data instanceof DeleteRowsEventData) {
                        DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) data;
                        String tableName = tableMap.get(deleteRowsEventData.getTableId());
//                        if (filterData(tableName)) {
//                            String eventKey = tableName + ".delete";
//                            for (Serializable[] row : deleteRowsEventData.getRows()) {
//                                String msg = JSON.toJSONString(new BinlogDTO(eventKey, row));
//                                log.info("delete data : {}", msg);
//                            }
//                        }
                    }
                }
            }
        });
        log.info("监听程序已启动...");
        client.connect();


    }


    private boolean filterData(String tableName){
        log.info("filterData current tableName : {}",tableName);
        if(tableName == null){
            return false;
        }

        List<String> includes = Arrays.asList(tableName.split(","));
        return includes.contains(tableName);
    }
}
